from tomcat

maintainer stuffie

copy target/scalatra-maven.war /usr/local/tomcat/webapps/scalatra-maven.war

expose 8080

CMD ["catalina.sh", "run"]

